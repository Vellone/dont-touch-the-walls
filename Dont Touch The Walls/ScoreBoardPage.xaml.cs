﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Dont_Touch_The_Walls
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ScoreBoardPage : Page
    {
        public Dictionary<string, double> playerScores = new Dictionary<string, double>();

        public ScoreBoardPage()
        {
            this.InitializeComponent();
            playerScores.Add("Anthony", 10);
            playerScores.Add("Fred", 8);
            playerScores.Add("Frank", 15);
            playerScores.Add("Eric", 30);
            playerScores.Add("Lucas", 20);
            playerScores.Add("Jess", 5);

            foreach (KeyValuePair<string, double> element in playerScores.OrderBy(key => key.Value))
            {
                LeaderBoardOutput.Text += $"{element.Key} : {element.Value} Deaths" + Environment.NewLine;
            }
        }

        private void OnBackToMainPage(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
    }
}
