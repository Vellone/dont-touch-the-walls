﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Touch_The_Walls
{
    public class Walls
    {
        public List<Walls> walls = new List<Walls>();

        private int _w;
        private int _h;
        private int _x;
        private int _y;

        public Walls(int w, int h, int x, int y)
        {
            _w = w;
            _h = h;
            _x = x;
            _y = y;
        }
    }
}
