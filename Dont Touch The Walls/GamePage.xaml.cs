﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Dont_Touch_The_Walls
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {
        private int gameState = 0;
        private int _canvasWidth, _canvasHeight;
        private int _playerWidth, _playerHeight;
        private int _nestWidth, _nestHeight;
        private int _stepSize = 5;
        private int _deaths = 0;

        public GamePage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Sets the fields for holding the width and the height of the container and the components
        /// </summary>
        private void SetDimensions()
        {
            _canvasWidth = (int)MainCanvas.ActualWidth;
            _canvasHeight = (int)MainCanvas.ActualHeight;
            _playerWidth = (int)BPlayer.ActualWidth;
            _playerHeight = (int)BPlayer.ActualHeight;
            _nestWidth = (int)ImgNest.ActualWidth;
            _nestHeight = (int)ImgNest.ActualHeight;
        }

        private void OnPageLoaded(object sender, RoutedEventArgs e)
        {
            SetDimensions();

            int xPosition = 0;
            int yPosition = 0;
            Canvas.SetLeft(ImgNest, xPosition);
            Canvas.SetTop(ImgNest, yPosition);

            Walls w1 = new Walls(250, 200, 0, 400);
            //walls.add(w1);

            Walls w2 = new Walls(250, 200, 0, 400);

        }

        private void _timer_Tick(object sender, object e)
        {
            // MoveSpriteToRandomLocation(ImgNest, _nestWidth, _nestHeight);
        }

        public void MoveSpriteToRandomLocation(UIElement sprite, int spriteWidth, int spriteHeight)
        {
            // int xPosition = _random.Next(0, _canvasWidth - _nestWidth);
            // int yPosition = _random.Next(0, _canvasHeight - _nestHeight);
            // Canvas.SetLeft(ImgNest, xPosition);
            // Canvas.SetTop(ImgNest, yPosition);
        }

        /// <summary>
        /// Moves the sprite in the upward direction by a predefined step size
        /// </summary>
        /// <param name="sprite">The sprite to be moved</param>
        private void MoveSpriteUp(UIElement sprite)
        {
            int newTop = (int)Canvas.GetTop(sprite) - _stepSize;
            if (newTop >= 0)
                Canvas.SetTop(sprite, newTop);
        }

        /// <summary>
        /// Moves the sprite in the downward direction by a predefined step size
        /// </summary>
        /// <param name="sprite">The sprite tho be moved</param>
        /// <param name="spriteHeight">The height of the sprite to be moved</param>
        private void MoveSpriteDown(UIElement sprite, int spriteHeight)
        {
            int newTop = (int)Canvas.GetTop(sprite) + _stepSize;
            if (newTop <= _canvasHeight - spriteHeight)
                Canvas.SetTop(sprite, newTop);
        }

        /// <summary>
        /// Moves the sprite in the left direction by a predefined step size
        /// </summary>
        /// <param name="sprite">The sprite to be moved</param>
        private void MoveSpriteLeft(UIElement sprite)
        {
            int newLeft = (int)Canvas.GetLeft(sprite) - _stepSize;
            if (newLeft >= 0)
                Canvas.SetLeft(sprite, newLeft);
        }

        /// <summary>
        /// Moves the sprite in the right direction by a predefined step size
        /// </summary>
        /// <param name="sprite">The sprite tho be moved</param>
        /// <param name="spriteWidth">The height of the sprite to be moved</param>
        private void MoveSpriteRight(UIElement sprite, int spriteWidth)
        {
            int newLeft = (int)Canvas.GetLeft(sprite) + _stepSize;
            if (newLeft <= _canvasWidth - spriteWidth)
                Canvas.SetLeft(sprite, newLeft);
        }

        /// <summary>
        /// Called when a key goes down on the Player Button
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="e">The event object</param>
        private void OnPlayerKeyDown(object sender, KeyRoutedEventArgs e)
        {
            switch (e.Key)
            {
                // set to W, A, S, D for now because i do not have arrow keys
                case VirtualKey.A:
                    MoveSpriteLeft(BPlayer);
                    break;
                case VirtualKey.D:
                    MoveSpriteRight(BPlayer, _playerWidth);
                    break;
                case VirtualKey.W:
                    MoveSpriteUp(BPlayer);
                    break;
                case VirtualKey.S:
                    MoveSpriteDown(BPlayer, _playerHeight);
                    break;
            }

            if (CollisionDetected())
            {
                gameState++;
                if (gameState == 5)
                {
                    DoGameOver();
                }
                DoNextLevel();
            }
        }

        /// <summary>
        /// Reflects the game over state in the UI
        /// </summary>
        private void DoGameOver()
        {
            ImgNest.Visibility = Visibility.Collapsed; //hidden
            BPlayer.IsEnabled = false; //cannot receive keyboard events
        }

        private void DoNextLevel()
        {
            Frame.Navigate(typeof(ScoreBoardPage));
        }

        /// <summary>
        /// Detected whether the player collided with the food
        /// </summary>
        /// <returns>True if there was a collision, false otherwise</returns>
        private bool CollisionDetected()
        {
            Rect playerRect = new Rect((int)Canvas.GetLeft(BPlayer), (int)Canvas.GetTop(BPlayer), _playerWidth,
                _playerHeight);
            Rect nestRect = new Rect((int)Canvas.GetLeft(ImgNest), (int)Canvas.GetTop(ImgNest), _nestWidth,
                _nestHeight);
            Rect intersection = RectHelper.Intersect(playerRect, nestRect);
            return !intersection.IsEmpty;
        }
    }
}
